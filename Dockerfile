FROM php:7.3-apache-stretch
RUN mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini
RUN sed -i 's/max_execution_time = 30/max_execution_time = 300/g' $PHP_INI_DIR/php.ini
RUN apt-get update
RUN apt-get install libpng-dev -yq
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install -j$(nproc) opcache
RUN docker-php-ext-enable opcache
RUN docker-php-ext-install -j$(nproc) mysqli
RUN docker-php-ext-install -j$(nproc) pdo_mysql
RUN a2enmod rewrite
RUN sed -i '/combined/a </Directory>'  /etc/apache2/sites-available/000-default.conf
RUN sed -i '/combined/a RewriteRule ^(.*)$ index.php?q=$1 [L,QSA]'  /etc/apache2/sites-available/000-default.conf
RUN sed -i '/combined/a RewriteCond %{REQUEST_FILENAME} !-d'  /etc/apache2/sites-available/000-default.conf
RUN sed -i '/combined/a RewriteCond %{REQUEST_FILENAME} !-f'  /etc/apache2/sites-available/000-default.conf
RUN sed -i '/combined/a RewriteBase /'  /etc/apache2/sites-available/000-default.conf
RUN sed -i '/combined/a RewriteEngine on'  /etc/apache2/sites-available/000-default.conf
RUN sed -i '/combined/a <Directory /var/www/html/>'  /etc/apache2/sites-available/000-default.conf
RUN apt-get install libzip-dev -yq
RUN docker-php-ext-install zip
RUN apt-get install libicu-dev -yq
RUN docker-php-ext-install intl
RUN apt-get install libxml2-dev -yq
RUN docker-php-ext-install xmlrpc
RUN docker-php-ext-install soap
RUN mkdir /var/wwwextra
RUN chown www-data:www-data /var/wwwextra
RUN chmod 755 /var/wwwextra
